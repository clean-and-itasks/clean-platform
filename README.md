# Clean Platform

This package is deprecated! If using modules still present in this package, consider moving them to a separate package.

The Clean Platform is a general purpose collection of libraries to supplement the Clean Standard Environment.
To increase maintainability and readability, these libraries follow a strict coding style guideline.
This guideline can be found in the file STANDARDS.txt and on the Clean Wiki. A listing of the conceived
API the Clean Platform will provide can be found in the file API.txt

More information about this project can be found on the Clean Wiki:
http://wiki.clean.cs.ru.nl/Clean_platform

## License

All original modules are provided under the same license as the Clean System,
the Simplified BSD License (2-clause BSD License, see [LICENSE](LICENCE)).

Some modules were ported from Haskell and they are provided the compatible
Haskell's 3-clause BSD license (see [LICENSE.BSD3](LICENCE.BSD3)).

- Data.Heap
