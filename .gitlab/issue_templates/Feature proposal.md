### Problem to solve

<!-- What problem do we solve? -->

### Intended users

<!-- What kind of applications will use this feature? You can give concrete examples. -->

### Proposal

<!-- How are we going to solve the problem? -->

### Documentation

<!-- Does this feature need to be documented, and if so, where? -->

### Acceptance criteria

<!-- Describe a MVP (minimum viable product). Further enhancements can follow later, but you can already create related issues for them if you want. -->

/label ~Enhancement / ~Feature
<!--
Pick one: features are really new; enhancements are improvements of existing functionality.
-->
