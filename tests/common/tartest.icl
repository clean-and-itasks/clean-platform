module tartest

import Data.Error
import Data.Functor

import Codec.Archive.Tar

import StdFile
import StdFunc
import StdList
import System.SysCall

Start w = unTarFile id "_tartest.tar" w
