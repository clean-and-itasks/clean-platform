## 0.4.0
- Remove: modules which were ported to other packages.

#### 0.3.44

- Enhancement: Prevent abort in `gBinaryDecode` on encoding with invalid constructor index.

#### 0.3.43

- Feature: add `_Posix.access` and corresponding flags.

#### 0.3.42

- Enhancement: Update queue.c: Allocate/copy memory for packet to be enqueued within the enqueue function for queues to be consistent with enqueueFront.

#### 0.3.41

- Enchancement: package queue.c/queue.h for use in other applications/libraries on linux-x64.

#### 0.3.40

- Fix: XML decoder now is able to parse comments, deal with attribute values
       enclosed in single quotes and does not return obsolete XMLText "" in
	   the decoding result when a space is added between tags.

#### 0.3.39

- Feature: add `Data.Map.ToList` to convert maps to overloaded lists.
- Feature: `Text.Parsers.Simple.Core`: add `someTill` and `manyTill`.
- Feature: `Text.Parsers.Simple.Chars`: add `pSingleQuote` and `pDoubleQuote`.

#### 0.3.38

- Feature: `Data.Map`: add `anyMap`.
- Feature: `Data.Map`: add overloaded `FromList`.

#### 0.3.37

- Feature: `Data.Set`: add `setToMaybe`.

#### 0.3.36

- Feature: `Data.List`: add `RemoveDupBy` for overloaded lists.
- Feature: `Data.List`: add `MaxListBy` for overloaded lists.
- Feature: `Data.List`: add `MinListBy` for overloaded lists.

#### 0.3.35

- Feature: `Data.Set`: add `maybeToSet` converting a `?|` value to a set.

#### 0.3.34

- Fix: System.AsyncIO: only evaluate the `onDisconnect` handler when the
          peer closes the connection, not when it is closed out of ones own
          initiative.

#### 0.3.33

- Fix: several bugs in implementation of Windows version of AsyncIO.

#### 0.3.32

- Fix: System.Asyncio (Posix): do not abort if signal interrupts call to `epoll_wait`.

#### 0.3.31

- Fix: include missing memory.o file in nitrile package added by v0.3.30.

#### 0.3.30 (BROKEN)

- Feature: `System.Memory`, add `memcpy_string_to_pointer_offset` and `memcpy_string_to_pointer_offset_st`
    which allow to copy a `String` to a pointer given an offset.

#### 0.3.29

- Fix: `System.AsyncIO`. use timeout argument provided to event loop for IOCP.

#### 0.3.28

- Fix: `System.AsyncIO`, make removing the write queue of a socket noop
       in case it is not present
       (listening sockets have no write queue, which could result in an error)
- Fix: Evaluate the `onError` callback before closing the socket in
       case a host could not be reached.

#### 0.3.27

- Feature: added support for keyboard handling and focus control for SVG images.

#### 0.3.26

- Feature: add `($&) :: a !(a -> b) -> b` to `Data.Func`.
- Feature: add `(<$&>) :: (f a) (a -> b) -> f b | Functor f` to
  `Data.Functor`.
- Change: `<$>` is no longer strict in its second argument (as was already the
  case for `fmap`). In most cases this has no effect, since `fmap` is defined
  as strict in this argument in most instance definitions.

#### 0.3.25

- Feature: `Data.Map`: add `takeLargestNEntries`.

#### 0.3.24

- Feature: add `gPrint`, `genShow` and `ggen` instances for `JSONNode`.

#### 0.3.23

 - Feature: `Data.Map`: add `partitionMapBy`, `arbitraryElementOf` and `allMap`.

#### 0.3.22

 - Feature: add `memcpy` and `memcpySt`.
 - Enhancement: allow operations on finalized values to yield a unique result.

#### 0.3.21

 - Enhancement: improve space complexity of `foldrArrWithKey` and `foldrArr` (used to be O(N), now O(1)).

#### 0.3.20

- Feature: add `System._Memory.memcpyPointerToString`/`memcpyPointerToStringSt`.
- Enhancement: optimise `System._Pointer.derefString`/`derefCharArray`.

#### 0.3.19

- Feature: `Data.Func`: Add `MapSt` for overloaded lists.
- Documentation: fix documentation of `System._Memory.memcpy_string_to_pointer`.
- Feature: add `System._Memory.memcmp`/`System._Memory.memcmpSt`.

#### 0.3.18

- Fix: fix stack overflow issues when decoding large strings with `GenBinary`.

#### 0.3.17

- Fix: fix real number representation of `instance toString JSONNode`.

#### 0.3.16

- Feature: add `PartitionGroup` (overloaded version of `partitionGroup`) to `Data.List`.
- Feature: add `Elems` (overloaded version of `elems`) to `Data.Map`.

#### 0.3.15

- Feature: add `flock` Posix syscall.

#### 0.3.14

- Fix: solve `ensureDirectoryExists` wrongly yielding a "directory already exists" error in case another process created the directory at the same time.

#### 0.3.13

- Change: remove extra text from `replaceCurrentProcess` error messages for consistency with other functions returning a `MaybeOSError`.

#### 0.3.12

- Fix: import _execvp function from ucrtbase as compiling programs which used `replaceCurrentProcess` on Windows would fail.

#### 0.3.11 (BROKEN ON WINDOWS-x64)

- Feature: add `replaceCurrentProcess` to replace the currently running Clean process with a different process.

#### 0.3.10

- Enhancement: allow arbitrary environment for function in Posix System._Process and System._Linux; do not restrict usage to World.
- Feature: add `getpid` system call.
- Enhancement: allow arbitrary environment for function in Posix System._Process; do not restrict usage to World.
- Feature: add `F_SETPIPE_SZ` constant.
- Feature: add Linux get_nprocs syscall.

#### 0.3.9

- Feature: add instance == CleanComment.
- Fix: remove trailing newline from `parseSingleLineDoc` result.
- Chore: Switch from deprecated lib-compiler-itasks to lib-compiler.

#### 0.3.8

- Fix: Switch back to lib-compiler-itasks to fix compiler crashes on Windows-x64.

#### 0.3.7 (BROKEN ON WINDOWS-x64)

- Chore: Switch from deprecated lib-compiler-itasks to lib-compiler.

#### 0.3.6

- Fix: Parsing XML without value for attribute of the form attr=""> now no longer returns a lexing error as this is valid XML.

### 0.3.5

- Feature: add `withFinalizedInt2`.

### 0.3.4

- Enhancement: add 32 bit version of `setStackTraceDepth` to `Debug.StackTrace`.

### 0.3.3

- Fix: fix `parseSingleLineDoc` in Clean.Doc when there are multiple lines;
  strip asterisk from each line.
- Fix: on POSIX, the subprocess run by `runProcessPty` now explicitly opens its
  tty to become a controlling tty (if `childControlsTty` is `True`), which is
  needed to run `/bin/sh` with job control (`set -m`), for example.
- Feature: add `partitionGroup`, which is like `groupBy` but uses
  `partition` instead of `span`.
- Fix: add support for `{^..}` lazy arrays and `<!-` strict list generator in
  the Clean pretty printer.

### 0.3.2

- Update: reinclude bsearch.o and wsubst.o in package as they were still used by Text.Unicode.UChar, which is used by eastwood.
- Fix: fix comment parser in Clean.Parse.Comments when the file contains string
  literals with escape sequences.

### 0.3.1

- Add missing function GetExitCodeThread to _WinBase dll imports.

## 0.3.0

- Fix: compile C dependencies for System.Process and System.Signal for i386 in
  the linux-x86 target.
- Fix: do not inherit all handles from the parent process in System.Process for
  Windows.
- Fix: prevent deadlocks in Windows `readPipeBlockingMulti` due to use of
  unsafe `TerminateThread`.
- Fix: fix segfault in Windows `readPipeBlockingMulti` if garbage collection
  runs while creating threads; this would cause a segfault because the array
  with pipe handles has been moved by the Clean run-time system.
- Fix: increase pipe buffer size on Windows to avoid deadlocks where the child
  process tries to write a message that does not fit in the pipe.
- Potential fix: removed write of 0 bytes to `stderr` in Windows
  `readPipeBlockingMulti`, which seems to cause deadlocks in some cases and is
  anyway not needed (we were never really sure why it was added).
- Fix: don't prepend `\r` to `\n` in stdout in Windows `callProcessAndPassIO`.
  (This lead to `\r..\r\n` sequences, which are hidden in GitLab CI traces.)
- Fix: truncate file after `copyFile` on Linux.

## 0.2.0

- Switch to nitrile.

## 22.5

### New features
- New module `Text.Unicode.Encodings.UTF8.GenHash`, Elroy Jumpertz (@elroyTop), !517
- New function `empty` in `Text.Unicode.Encodings.UTF8`, Elroy Jumpertz (@elroyTop), !518

## 22.4

### New features
- Add `genericDescriptorType` and `toString` for `GenType`, Mart Lubbers (mart@cs.ru.nl), !505
- `Text.Unicode.Encodings.UTF8`: Add `==`, `<` and `gPrint` instances for `:: UTF8`; `Text.Unicode.Encodings.UTF8.Gast`: Add module containing `genShow` instance for `:: UTF8`, Gijs Alberts (gijs@gijsalberts.nl), !513

### API changes
- `Text.Unicode.Encodings.UTF8`: replaced `fromString UTF8` by `utf8StringCorrespondingTo`, Steffen Michels (steffen@top-software.nl), !512

## 22.3

### New features
- `Debug.StackTrace`: Add `setStackTraceDepth` to customize the amount of stack frames that are printed to stderr on a program crash, John van Groningen (and Gijs Alberts (gijs@gijsalberts.nl)), !495

## 22.2

No changes.

## 22.1

### New features
- Data.OrdList: Add `Merge` and `MergeBy`, Steffen Michels (steffen@top-software.nl) !492
- Added `toString` instance for `:: Timespec`, Gijs Alberts (gijs@gijsalberts.nl), !491

## 22.0

### New features
- Added `Data.Bool` module which exports `(-->) infixr 1` (implication) and `(<-->) infixr 1` (equivalence), Gijs Alberts (gijs@gijsalberts.nl), !483
- Added `Data.Either.Gast` module which exports `derive ggen Either` and `derive genShow Either`. Export `derive gPrint Either` in `Data.Either.GenPrint`, Gijs Alberts (gijs@gijsalberts.nl), !484

### Bug fixes
- Fixed System.Environment on Windows, Camil Staps (info@camilstaps.nl), !481

## 21.7

### New features
- added `Data.List.NonEmpty`, Erin van der Veen (@ErinvanderVeen), !470
- `Data.Error`: export `mapError` function, Gijs Alberts (gijs@gijsalberts.nl), !472
- `Data.Error`: implement `MonadFail` instance for `MaybeError String`, Erin van der Veen (@ErinvanderVeen), !473
- `Text.Html`: export `==` instance for `HtmlTag`, Gijs Alberts (gijs@gijsalberts.nl), !475
- `Data.Encoding.Binary`: export `gBinaryEncode`/`gBinaryEncodingSize`/`gBinaryDecode` for head strict/spine strict/head spine strict lists, !477
- Added `toString` instance to type `Either`, Elroy Jumpertz (@elroyTop), !478
- `System.Process`: Export `callProcessWithOutput` and `ProcessResult` record, Gijs Alberts (gijs@gijsalberts.nl), !479

### Other
- 32-bit platforms are not actively supported anymore, Steffen Michels (@smichels), !469

## 21.6

### New features
- Added `Text.CSV.foldedCsvRowsWith`, Steffen Michels (@smichels), !465

## 21.5

### New features
- Added System.AsyncIO module which supports performing network I/O operations in an asynchronous manner. For more information, see the System.AsyncIO module., Gijs Alberts (gijs@gijsalberts.nl), !451

## 21.4

### Bug fixes
- Bug fixes in Clean.Types.Tree and Clean.Types.Unify. `addType` has an extra argument for the type, prepared as a 'left' type for unification., Camil Staps (info@camilstaps.nl), !444

## 21.3

### New features
- Added `fromAscList`, `fromDescList`, `fromDistinctAscList`, and `fromDistinctDescList` to Data.Set that provide increased performance for less versatility, Erin van der Veen (erin@erinvanderveen.nl), !435
- Added functions `isSpecialisingUnifier`, `` generalises` ``, `` specialises` ``, and `` isomorphic_to` ``, which make it possible to check whether types that are already prepared for unification with `prepare_unification` specialise / generalise / are isomorphic to each other, Camil Staps (info@camilstaps.nl), !440

### Bug fixes
- Fixed bugs in unification of types with multiple scopes of universally quantified variables (Clean.Types.Unify), Camil Staps (info@camilstaps.nl), !440

## 21.2

### API changes
- Added support for non-standard notation of builtin types (e.g. `[] a`, but also `[]` and `[] a b`) to the Clean.Types parser and pretty-printer, Camil Staps (info@camilstaps.nl), !432

### Bug fixes
- Fixed timespecSleep for windows, Mart Lubbers (mart@cs.ru.nl), !433
